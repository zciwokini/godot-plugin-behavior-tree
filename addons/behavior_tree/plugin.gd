@tool
class_name BehaviorTreePlugin extends EditorPlugin


#region Properties

const plugin_name: String = "Behavior Tree Plugin"
const plugin_root: String = "res://addons/behavior_tree/"

var singletons: Dictionary = {}

var types: Array[Dictionary] = [
	{
		"name": "BehaviorTreeLogic",
		"base": "Resource",
		"script": load(plugin_root + "scripts/nodes/logic_node.gd")
	},
	{
		"name": "BehaviorTree",
		"base": "Node",
		"script": load(plugin_root + "scripts/behavior_tree.gd")
	},
]

var settings := [
	{
		"name": "editor/nodes_folders",
		"default_value": PackedStringArray([
			"res://addons/behavior_tree/scripts/nodes/default",
		]),
	},
	{ "name": "editor/show_grid", "default_value": true },
	{ "name": "editor/snapping_enabled", "default_value": true },
	{ "name": "editor/snapping_distance", "default_value": 20 },
	{ "name": "editor/minimap_enabled", "default_value": false },
	{ "name": "editor/show_grid_buttons", "default_value": false },
	{ "name": "editor/show_minimap_button", "default_value": false },
	{ "name": "editor/show_zoom_buttons", "default_value": false },
	{ "name": "editor/show_arrange_button", "default_value": false },
]

var editor: Control

#endregion Properties


#region Virtual methods

func _enter_tree() -> void:
	message("Loading...")
	load_settings()
	load_types()
	load_singletons()
	load_controls()
	message("Loaded")
	#var theme: Theme = EditorInterface.get_editor_theme()
	#for icon_type in theme.get_icon_type_list():
		#for icon_name in theme.get_icon_list(icon_type):
			#print("%s : %s" % [icon_type, icon_name])


func _exit_tree() -> void:
	message("Unloading...")
	unload_controls()
	unload_singletons()
	unload_types()
	unload_settings()
	message("Unloaded")

#endregion Virtual methods


#region Methods

func load_settings() -> void:
	pass
	for setting in settings:
		var setting_name: String = "editor/behavior_tree/" + setting["name"]
		if not ProjectSettings.has_setting(setting_name):
			ProjectSettings.set_setting(setting_name, setting.get("default_value"))
		ProjectSettings.set_initial_value(setting_name, setting.get("default_value"))

	ProjectSettings.save()
	#project_settings_changed.connect(ProjectSettings.save)


func unload_settings() -> void:
	pass


func load_singletons() -> void:
	for singleton in singletons:
		add_autoload_singleton(singleton, singletons[singleton])


func unload_singletons() -> void:
	for singleton in singletons:
		remove_autoload_singleton(singleton)


func load_types() -> void:
	for type in types:
		add_custom_type(
			type.get("name"),
			type.get("base"),
			type.get("script"),
			type.get("icon"),
		)


func unload_types() -> void:
	for type in types:
		remove_custom_type(type.get("name"))


func load_controls() -> void:
	var editor_res = load(plugin_root + "scenes/editor.tscn")

	if not editor_res:
		printerr("The Behavior Tree editor could not be loaded! Please report this bug!")
		return

	editor = editor_res.instantiate()
	add_control_to_bottom_panel(editor, "Behavior Tree")


func unload_controls() -> void:
	if not editor:
		return

	remove_control_from_bottom_panel(editor)


func message(text: String) -> void:
	print("%s: %s" % [plugin_name, text])

#endregion Methods
