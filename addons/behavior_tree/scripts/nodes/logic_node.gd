class_name BTLogic extends BTNode


@export var root_tick_node: BTRootNode
@export var root_timer_node: BTRootTimerNode
@export var orphan_nodes: Array[BTNode]



func _init() -> void:
	name = "Logic"


func tick(actor: Node, blackboard: Dictionary) -> int:
	var result: int
	return root_tick_node.tick(actor, blackboard)
#	for node in root_tick_nodes:
#		result = node.tick(actor, blackboard)
#		if result == STATE.FAILED:
#			return result
#
#	return STATE.SUCCESS


func get_graph_node() -> GraphNode:
	return BTGraphNode.new()\
		.set_graph_title(name)\
		.add_input_slot(BTTickSlot.new())\
		.add_input_slot(BTVariantSlot.new("Logic Path"))


func save() -> void:
	pass
