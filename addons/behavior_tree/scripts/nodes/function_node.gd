class_name BTNodeFunction extends BTNode


var callback: Callable


func _init(callback: Callable = func(): pass) -> void:
	self.callback = callback
	name = "Function"


func tick(actor: Node, blackboard: Dictionary) -> int:
	return callback.callv([actor, blackboard])
