class_name BTNode extends Resource


signal color_changed(color: Color)


enum STATE {
	SUCCESS = 0,
	FAILED = 1,
#	RUNNING = 2 # Does not work, beacause it is not implemented yet
}


@export var input_slots: Array[BTSlot] = []
@export var output_slots: Array[BTSlot] = []
@export var position: Vector2 = Vector2.ZERO

var group: String = "Nodes"
var name: String = "Node"
var icon: Texture2D = null


func tick(actor: Node, blackboard: Dictionary) -> STATE:
	return STATE.SUCCESS


func set_value(field: StringName, value: Variant) -> void:
	pass


func get_value(field: StringName) -> Variant:
	return null


func get_graph_node() -> GraphNode:
	var graph_node := BTGraphNode.new()\
		.set_graph_title(name)\
		.add_input_slot(BTTickSlot.new())

	return graph_node
