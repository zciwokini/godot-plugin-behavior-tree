class_name BTVariantNode extends BTNode


func _init() -> void:
	group = "Values"
	name = "Variant"


func get_graph_node() -> GraphNode:
	return BTGraphNode.new()\
		.set_graph_title(name)\
		.add_output_slot(BTVariantSlot.new(""))
