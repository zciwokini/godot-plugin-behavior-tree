class_name BTVector3Node extends BTVariantNode


#region Virtual methods

func _init() -> void:
	group = "Values"
	name = "Vector3"
	icon = EditorInterface.get_editor_theme().get_icon("Vector3", "EditorIcons")

#endregion Virtual methods


#region Methods

func get_graph_node() -> GraphNode:
	var graph_node := BTGraphNode.new()\
		.set_graph_title(name)\
		.add_output_slot(BTVector3Slot.new(""))

	return graph_node

#endregion Methods
