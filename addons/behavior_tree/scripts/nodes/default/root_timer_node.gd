class_name BTRootTimerNode extends BTRootNode



@export var time: float



func _init(time: float = 1.0) -> void:
	self.time = time
	group = "Roots"
	name = "Timer Root"
	icon = EditorInterface.get_editor_theme().get_icon("Timer", "EditorIcons")


func get_timer_node() -> Timer:
	var timer := Timer.new()
	timer.wait_time = time
	timer.autostart = true
	return timer


func get_graph_node() -> GraphNode:
	var graph_node := BTGraphNode.new()\
		.set_graph_title(name)\
		.add_output_slot(BTTickSlot.new().set_is_multiple(true))\
		.add_input_slot(BTFloatSlot.new("Time"))

	return graph_node
