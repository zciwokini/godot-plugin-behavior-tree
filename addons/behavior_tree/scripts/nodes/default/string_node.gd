@tool
class_name BTStringNode extends BTVariantNode


#region Virtual methods

func _init() -> void:
	group = "Values"
	name = "String"
	icon = EditorInterface.get_editor_theme().get_icon("String", "EditorIcons")

#endregion Virtual methods


#region Methods

func get_graph_node() -> GraphNode:
	var graph_node := BTGraphNode.new()\
		.set_graph_title(name)\
		.add_output_slot(BTStringSlot.new(""))

	return graph_node

#endregion Methods
