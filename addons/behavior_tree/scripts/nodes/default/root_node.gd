class_name BTRootNode extends BTNode



@export var nodes: Array[BTNode]



func _init() -> void:
	group = "Roots"
	name = "Manual Root"
	icon = EditorInterface.get_editor_theme().get_icon("InputEventMouseButton", "EditorIcons")


func tick(actor: Node, blackboard: Dictionary) -> int:
	for node in nodes:
		node.tick(actor, blackboard)
	return 0


func get_graph_node() -> GraphNode:
	return BTGraphNode.new()\
		.set_graph_title(name)\
		.add_output_slot(BTTickSlot.new().set_is_multiple(true))\
		.set_tooltip("Just a tooltip")
