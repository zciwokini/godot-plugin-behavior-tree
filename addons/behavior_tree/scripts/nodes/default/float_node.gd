@tool
class_name BTFloatNode extends BTVariantNode


#region Virtual methods

func _init() -> void:
	group = "Values"
	name = "Float"
	icon = EditorInterface.get_editor_theme().get_icon("float", "EditorIcons")

#endregion Virtual methods


#region Methods

func get_graph_node() -> GraphNode:
	var graph_node := BTGraphNode.new()\
		.set_graph_title(name)\
		.add_output_slot(BTFloatSlot.new(""))

	return graph_node

#endregion Methods
