class_name BTPrintNode extends BTNode


func _init() -> void:
	name = "Print"
	icon = EditorInterface.get_editor_theme().get_icon("NodeInfo", "EditorIcons")


func tick(actor: Node, blackboard: Dictionary) -> int:
	print("Hello world")
	return STATE.SUCCESS


func get_graph_node() -> GraphNode:
	return BTGraphNode.new()\
		.set_graph_title(name)\
		.add_input_slot(BTTickSlot.new())\
		.add_input_slot(BTStringSlot.new("Message"))
