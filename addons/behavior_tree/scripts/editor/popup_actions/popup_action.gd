class_name PopupAction extends RefCounted


#region Properties

var name: String
var group: String
var callback: Callable
var visibility_condition: Callable

#endregion Properties


#region Virtual methods

func _init(
	name: String = "",
	group: String = ""
) -> void:
	self.name = name
	self.group = group
	self.callback = func(): pass
	self.visibility_condition = func(): return true

#endregion Virtual methods


#region Methods

func set_callback(callback: Callable) -> PopupAction:
	self.callback = callback
	return self


func set_visibility_condition(callback: Callable) -> PopupAction:
	self.visibility_condition = callback
	return self

#endregion Methods
