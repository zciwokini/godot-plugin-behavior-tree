@tool
class_name ActionsPopupMenu extends PopupMenu


#region Signals

signal action_selected(action: PopupAction)

#endregion Signals


#region Properties

var actions: Dictionary
var _actions_count: int = 0
var last_popup_position: Vector2i

#endregion Properties


#region Virtual methods

func _ready() -> void:
	id_pressed.connect(_on_id_pressed)

#endregion Virtual methods


#region Methods

func add_action(action: PopupAction) -> void:
	if not action.group in actions:
		actions[action.group] = {}

	actions[action.group][_actions_count] = action
	_actions_count += 1


func get_action_by_id(id: int) -> PopupAction:
	for action_group in actions:
		for action_id in actions[action_group]:
			if action_id == id:
				return actions[action_group][action_id]
	return null


func refresh_items() -> void:
	clear()
	for action_group in actions:
		var visible_actions := 0
		for id in actions[action_group]:
			var action: PopupAction = actions[action_group][id]
			if action.visibility_condition.call():
				visible_actions += 1

		if visible_actions > 0 and action_group != "":
			add_separator(action_group)

		for id in actions[action_group]:
			var action: PopupAction = actions[action_group][id]
			if action.visibility_condition.call():
				add_item(action.name, id)


func popup_at(new_position: Vector2) -> void:
	refresh_items()
	reset_size()
	position = new_position
	last_popup_position = position
	popup()

#endregion Methods


#region Event methods

func _on_id_pressed(id: int) -> void:
	var action: PopupAction = get_action_by_id(id)
#	action.callback.call()
	action_selected.emit(action)

#endregion
