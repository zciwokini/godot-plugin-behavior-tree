@tool
class_name BTLogicEditor extends GraphEdit


# ----------
# Properties
# ----------

var editor: BehaviorTreeEditor
var logic_saver: BTLogicSaver

@onready var actions_popup_menu: ActionsPopupMenu
@onready var add_node_popup: AddNodePopup

var _actions: Array[Callable] = []


# ---------------
# Virtual methods
# ---------------

func _ready() -> void:
	actions_popup_menu = find_parent("Editor").find_child("ActionsPopupMenu", true, false)
	add_node_popup = find_parent("Editor").find_child("AddNodePopup", true, false)
	set_anchors_preset(PRESET_FULL_RECT)
	size_flags_horizontal = SIZE_EXPAND_FILL

	var menu: HBoxContainer = get_menu_hbox()
	var save_button = BuiltinIconButton.new("EditorIcons", "Save")
	save_button.tooltip_text = "Save"
	menu.add_child(save_button)

	add_node_popup.node_selected.connect(_on_node_selected)
	actions_popup_menu.action_selected.connect(_on_action_selected)

	popup_request.connect(_on_popup_request)
	delete_nodes_request.connect(_on_close_nodes_request)
	connection_request.connect(_on_connection_request)
	connection_from_empty.connect(_on_connection_from_empty)
	connection_to_empty.connect(_on_connection_to_empty)
	ProjectSettings.settings_changed.connect(_on_settings_changed)
	_on_settings_changed()


func _physics_process(delta: float) -> void:
	for action in _actions:
		_process_action()


func _process_action() -> void:
	var action = _actions.pop_front()
	if action is Callable:
		action.call()
	else:
		set_physics_process(false)


func _process_actions() -> void:
	for action in _actions:
		_process_action()



# -------
# Methods
# -------

func add_action_front(action: Callable) -> void:
	_actions.push_front(action)
	set_physics_process(true)


func add_actions_front(actions: Array[Callable]) -> void:
	for action in actions:
		add_action_front(action)
	set_physics_process(true)


func add_action_back(action: Callable) -> void:
	_actions.push_back(action)
	set_physics_process(true)


func add_actions_back(actions: Array[Callable]) -> void:
	for action in actions:
		add_action_back(action)
	set_physics_process(true)


func add_node(graph_node: GraphNode) -> void:
	add_child(graph_node)
	var window: Window = get_viewport().get_window()
	var screen_decor_offset := window.get_size_with_decorations() - window.size
	var node_size_offset := (graph_node.size / 2)

	var request_position: Vector2 = scroll_offset + Vector2(actions_popup_menu.last_popup_position) - Vector2(screen_decor_offset) - Vector2(global_position)

	var node_position := Vector2(request_position) / zoom - node_size_offset
	if snapping_enabled:
		node_position = node_position.snapped(Vector2(snapping_distance, snapping_distance))

	graph_node.position_offset = node_position

	graph_node.delete_request.connect(
		func(): add_action_back(delete_node.bind(graph_node.name))
	)


func get_connections_from_graph_node(node_name: StringName) -> void:
	var connections: Array[Dictionary] = []

	for c in get_connection_list():
		if c.from_node == node_name:
			connections.push_back(c)

	connections.sort_custom(func(a, b): return a.to_port > b.to_port)
	connections.sort_custom(func(a, b): return a.from_port > b.from_port)


func delete_node(node_name: StringName, notify: bool = true):
	var node: GraphNode = get_node_or_null(str(node_name))

	if not node:
		return

	add_action_front(disconnect_node_slots.bind(node_name, notify))
	add_action_back(node.queue_free)


func delete_selected_nodes() -> void:
	for node in get_children():
		if node is BTGraphNode and node.selected:
			add_action_front(
				delete_node.bind(node.name)
			)


func disconnect_node_input_slot(node_name: StringName, port: int, notify: bool = true):
	if not get_node_or_null(str(node_name)):
		return

	for c in get_connection_list():
		if c.to_node == node_name and c.to_port == port:
			if notify:
				add_action_front(
					notify_disconnection.bind([c.from_node, c.to_node], c.from_node, c.from_port, c.to_node, c.to_port)
				)
			add_action_front(
				disconnect_node.bind(c.from_node, c.from_port, c.to_node, c.to_port)
			)


func disconnect_node_output_slot(node_name: StringName, port: int, notify: bool = true):
	if not get_node_or_null(str(node_name)):
		return

	for c in get_connection_list():
		if c.from_node == node_name and c.from_port == port:
			if notify:
				add_action_front(
					notify_disconnection.bind([c.from_node, c.to_node], c.from_node, c.from_port, c.to_node, c.to_port)
				)
			add_action_front(
				disconnect_node.bind(c.from_node, c.from_port, c.to_node, c.to_port)
			)


func disconnect_node_slots(node_name: StringName, notify: bool = true):
	var dc_cons: Array[Dictionary] = []
	for c in get_connection_list():
		if c.from_node == node_name or c.to_node == node_name:
			dc_cons.push_back(c)

	dc_cons.sort_custom(func(a, b): return a.to_port < b.to_port)
	dc_cons.sort_custom(func(a, b): return a.from_port < b.from_port)

	for c in dc_cons:
		if notify:
			add_action_front(
				notify_disconnection.bind([c.from_node, c.to_node], c.from_node, c.from_port, c.to_node, c.to_port)
			)
		add_action_front(
			disconnect_node.bind(c.from_node, c.from_port, c.to_node, c.to_port)
		)


func move_from_connection(from: StringName, old_port: int, new_port: int) -> void:
	for c in get_connection_list():
		if c.from_node == from and c.from_port == old_port:
			add_action_front(
				connect_node.bind(c.from_node, new_port, c.to_node, c.to_port)
			)
			add_action_front(
				disconnect_node_output_slot.bind(from, old_port, false)
			)


func move_to_connection(to: StringName, old_port: int, new_port: int) -> void:
	for c in get_connection_list():
		if c.to_node == to and c.to_port == old_port:
			add_actions_front([
				connect_node.bind(c.from_node, c.from_port, c.to_node, new_port),
				disconnect_node_input_slot.bind(c.to_node, old_port, false),
			])


func notify_disconnection(node_names: Array, from: StringName, from_port: int, to: StringName, to_port: int) -> void:
	var connection: Dictionary = {
		from = from, from_port = from_port, to = to, to_port = to_port
	}

	for node_name in node_names:
		var node: BTGraphNode = get_node_or_null(str(node_name))
		if node:
			if node_name == from:
				node._on_from_port_disconnected(connection)
			elif node_name == to:
				node._on_to_port_disconnected(connection)


func notify_connection(node_names: Array, from: StringName, from_port: int, to: StringName, to_port: int) -> void:
	var connection: Dictionary = {
		from = from, from_port = from_port, to = to, to_port = to_port
	}

	for node_name in node_names:
		var node: BTGraphNode = get_node_or_null(str(node_name))
		if node:
			if node_name == from:
				node._on_from_port_connected(connection)
			elif node_name == to:
				node._on_to_port_connected(connection)


func _on_settings_changed() -> void:
	show_grid = get_setting("editor/show_grid", true)
	show_grid_buttons = get_setting("editor/show_grid_buttons", false)
	snapping_enabled = get_setting("editor/snapping_enabled", true)
	snapping_distance = get_setting("editor/snapping_distance", 20)
	minimap_enabled = get_setting("editor/minimap_enabled", false)
	show_minimap_button = get_setting("editor/show_minimap_button", false)
	show_zoom_buttons = get_setting("editor/show_zoom_buttons", false)
	show_arrange_button = get_setting("editor/show_arrange_button", false)



# --------------
# Helper methods
# --------------

func get_selected_nodes_count() -> int:
	var count := 0

	for node in get_children():
		if not node is GraphNode:
			continue

		if node.selected:
			count += 1

	return count


func get_setting(name: String, default_value: Variant) -> Variant:
	return ProjectSettings.get_setting("editor/behavior_tree/%s" % name, default_value)


# ------
# Events
# ------

func _on_action_selected(action: PopupAction) -> void:
	if not visible:
		return

	action.callback.call()


func _on_connection_request(from: StringName, from_port: int, to: StringName, to_port: int) -> void:
	if from == to or is_node_connected(from, from_port, to, to_port):
		return

	var is_moved: bool = false

	for c in get_connection_list():
		if c.from_node == from and c.from_port == from_port:
			is_moved = true

			add_actions_back([
				disconnect_node.bind(c.from_node, c.from_port, c.to_node, c.to_port),
				notify_disconnection.bind([c.to_node], c.from_node, c.from_port, c.to_node, c.to_port),
			])
			break

	add_actions_back([
		connect_node.bind(from, from_port, to, to_port),
		notify_connection.bind([to] if is_moved else [from, to], from, from_port, to, to_port),
	])


func _on_connection_from_empty(from: StringName, from_port: int, release_position: Vector2) -> void:
	add_action_back(
		disconnect_node_input_slot.bind(from, from_port)
	)


func _on_connection_to_empty(to: StringName, to_port: int, release_position: Vector2):
	add_action_back(
		disconnect_node_output_slot.bind(to, to_port)
	)


func _on_close_nodes_request(node_names: Array[StringName]) -> void:
	for node_name in node_names:
		add_action_back(
			delete_node.bind(node_name)
		)


func _on_popup_request(position: Vector2) -> void:
	actions_popup_menu.popup_at(
		get_screen_position() + get_local_mouse_position()
	)


func _on_node_selected(node_name: String) -> void:
	if not visible:
		return

	var node_class: Script = editor.behavior_tree_nodes[node_name]
	var node: BTNode = node_class.new()
	var graph_node: GraphNode = node.get_graph_node()
	graph_node.bt_node = node

	add_action_back(
		add_node.bind(graph_node)
	)

	graph_node.move_from_connection.connect(
		func(from: StringName, old_port: int, new_port: int):
			add_action_front(
				move_from_connection.bind(from, old_port, new_port)
			)
	)

	graph_node.move_to_connection.connect(
		func(to: StringName, old_port: int, new_port: int):
			add_action_front(
				move_to_connection.bind(to, old_port, new_port)
			)
	)

	graph_node.change_slot_size.connect(
		func(slot: BTSlot, value: int):
			add_action_front(
				func(): slot.extra_slots += value
			)
	)
