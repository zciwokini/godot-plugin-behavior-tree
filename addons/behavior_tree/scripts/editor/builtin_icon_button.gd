@tool
class_name BuiltinIconButton extends Button


#region Properties

@export var icon_type: String = ""
@export var icon_name: String = ""

#endregion Properties


#region Virtual methods

func _init(icon_type: String = "", icon_name: String = "") -> void:
	self.icon_type = icon_type
	self.icon_name = icon_name


func _ready() -> void:
	refresh_icon()
	EditorInterface.get_base_control().theme_changed.connect(refresh_icon)

#endregion Virtual methods


#region Methods

func refresh_icon() -> void:
	icon = EditorInterface.get_editor_theme().get_icon(icon_name, icon_type)

#endregion Methods
