@tool
class_name BehaviorTreeEditorTabs extends TabBar



func _ready() -> void:
	for i in range(tab_count - 1, -1, -1):
		remove_tab(i)
