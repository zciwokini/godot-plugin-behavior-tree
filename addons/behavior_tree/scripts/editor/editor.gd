@tool
class_name BehaviorTreeEditor extends Control


#region Properties

@onready var tabs: TabBar = $"%Tabs"
@onready var graph_editors: Control = $"%GraphEditors"
@onready var actions_popup_menu: ActionsPopupMenu = $"%ActionsPopupMenu"
@onready var add_node_popup_menu: AddNodePopup = $"%AddNodePopup"

var active_logic_editor: BTLogicEditor

var behavior_tree_node_folders := []

var behavior_tree_nodes := {}

#endregion Properties


#region Virtual methods

func _ready() -> void:
	tabs.tab_changed.connect(_on_tab_changed)
	var nodes_folders = ProjectSettings.get_setting("editor/behavior_tree/editor/nodes_folders", [])
	for nodes_folder in nodes_folders:
		load_behavior_tree_nodes_from_folder(nodes_folder)

	open_behavior_tree("debug_1")
	open_behavior_tree("debug_2")

	actions_popup_menu.add_action(
		PopupAction.new("Add node")\
			.set_callback(
				func(): add_node_popup_menu.popup_at(get_screen_position() + get_local_mouse_position())
			)
	)

	actions_popup_menu.add_action(
		PopupAction.new("Save")\
			.set_callback(func(): pass)
	)

	actions_popup_menu.add_action(
		PopupAction.new("Delete", "Selected")\
			.set_callback(delete_selected_nodes)\
			.set_visibility_condition(is_any_node_selected)
	)

#endregion Virtual methods


#region Methods

func open_behavior_tree(resource) -> void:
	tabs.add_tab(resource)
	var graph_editor := BTLogicEditor.new()
	graph_editor.name = resource
	graph_editor.editor = self
	if active_logic_editor:
		active_logic_editor.visible = false
	active_logic_editor = graph_editor
	active_logic_editor.visible = true
	graph_editors.add_child.call_deferred(graph_editor)
	tabs.set_deferred("current_tab", tabs.tab_count - 1)


func load_behavior_tree_nodes_from_folder(path: String, recursive: bool = true) -> void:
	var files := DirAccess.get_files_at(path)

	for file in files:
		var node_script = load(path.path_join(file))
		if not node_script is Script:
			continue

		var node = node_script.new()
		if not node is BTNode:
			continue

		add_node_popup_menu.add_node(node)
		behavior_tree_nodes[node.name] = node_script

	var directories := DirAccess.get_directories_at(path)

	for directory in directories:
		load_behavior_tree_nodes_from_folder(
			path.path_join(String(directory)),
			recursive
		)


func delete_selected_nodes() -> void:
	if active_logic_editor:
		active_logic_editor.delete_selected_nodes()


func is_any_node_selected() -> bool:
	if active_logic_editor:
		return active_logic_editor.get_selected_nodes_count() > 0
	return false

#endregion Methods


#region Event methods

func _on_tab_changed(id: int) -> void:
	if active_logic_editor:
		active_logic_editor.visible = false

	if len(graph_editors.get_children()) == 0:
		return

	active_logic_editor = graph_editors.get_node(tabs.get_tab_title(id))
	if active_logic_editor:
		active_logic_editor.visible = true

#endregion Event methods
