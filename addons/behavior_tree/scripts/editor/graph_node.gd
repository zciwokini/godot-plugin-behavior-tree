class_name BTGraphNode extends GraphNode


#region Properties

var bt_node: BTNode

var input_slots: Array[BTSlot] = []
var output_slots: Array[BTSlot] = []

var font_color: Color
var icon: Texture2D

#endregion Properties


#region Signals

signal move_from_connection(from: String, old_port: int, new_port: int)
signal move_to_connection(to: String, old_port: int, new_port: int)
signal change_slot_size(slot: BTSlot, value: int)

#endregion Signals


#region Virtual methods

func _ready() -> void:
	add_theme_constant_override("port_h_offset", 10)
	refresh()
	EditorInterface.get_base_control().theme_changed.connect(refresh)
	#get_tree().root.theme_changed.connect(refresh)
	position_offset_changed.connect(_on_position_offset_changed)
	resize_request.connect(_on_resize_request)


func _gui_input(event: InputEvent):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_RIGHT:
			selected = true

#endregion Virtual methods


#region Methods

func refresh() -> void:
	font_color = EditorInterface.get_base_control().get_theme_color("font_color", "Label")
	bt_node.color_changed.emit(font_color)
	generate_slots()


func generate_slots() -> void:
	clear_all_slots()

	# Clear all children
	for child in get_children():
		for sub_child in child.get_children():
			for sub_sub_child in sub_child.get_children():
				sub_child.remove_child(sub_sub_child)
		remove_child(child)

	var input_controls: Array[Control] = []
	var output_controls: Array[Control] = []

	# Gather all input/output slot controls
	for input_slot in input_slots:
		input_controls.append_array(input_slot.get_input_controls())
	for output_slot in output_slots:
		output_controls.append_array(output_slot.get_output_controls())

	var max_i: int = max(len(input_controls), len(output_controls))

	for i in range(max_i):
		var is_in: bool = len(input_controls) > i
		var is_out: bool = len(output_controls) > i

		var in_c: Control = input_controls[i] if is_in else Control.new()
		var out_c: Control = output_controls[i] if is_out else Control.new()
		var in_type: int = 0
		var out_type: int = 0
		var in_icon: Texture2D = null
		var out_icon: Texture2D = null

		var hbox := HBoxContainer.new()
		if is_in and is_out:
			hbox.add_theme_constant_override("separation", 20)
		hbox.add_child(in_c)
		hbox.add_child(out_c)

		var row := MarginContainer.new()
		if is_in:
			row.add_theme_constant_override("margin_left", 10)
		if is_out:
			row.add_theme_constant_override("margin_right", 10)
		row.add_child(hbox)
		add_child(row)

		row.set_meta("left", is_in)
		row.set_meta("right", is_out)

		if is_in:
			var slot = in_c.get_meta("slot")
			row.set_meta("left_slot", slot)
			in_type = slot.type
			in_icon = slot.icon

		if is_out:
			var slot = out_c.get_meta("slot")
			row.set_meta("right_slot", slot)
			out_type = slot.type
			out_icon = slot.icon

		set_slot(
			i,
			is_in, in_type, in_c.get_meta("color", font_color),
			is_out, out_type, out_c.get_meta("color", font_color),
			in_icon, out_icon
		)

	reset_size()


func set_graph_title(title: String) -> BTGraphNode:
	self.title = title
	return self


func set_tooltip(text: String) -> BTGraphNode:
	tooltip_text = text
	return self


func add_input_slot(slot: BTSlot) -> BTGraphNode:
	input_slots.push_back(slot)
	slot.size_changed.connect(reset_size)
	slot.slot_count_changed.connect(generate_slots)
	return self


func add_output_slot(slot: BTSlot) -> BTGraphNode:
	output_slots.push_back(slot)
	slot.size_changed.connect(reset_size)
	slot.slot_count_changed.connect(generate_slots)
	return self


func get_left_slot_controls() -> Array[Node]:
	return get_children().filter(
		func(slot_control):
			return slot_control.get_meta("left", false)
	)


func get_right_slot_controls() -> Array[Node]:
	return get_children().filter(
		func(slot_control):
			return slot_control.get_meta("right", false)
	)

#endregion Methods


#region Event methods

func _on_position_offset_changed() -> void:
	pass


func _on_resize_request(new_minsize: Vector2) -> void:
	size = new_minsize


func _on_from_port_connected(connection: Dictionary) -> void:
	var slot_control: Control = get_right_slot_controls()[connection.from_port]
	var slot: BTSlot = slot_control.get_meta("right_slot")

	slot.is_from_port_connected = true

	if not slot.is_multiple:
		return

	var left_controls: Array[Node] = get_left_slot_controls()
	var right_controls: Array[Node] = get_right_slot_controls()

	for i in range(right_controls.size() - 1, connection.from_port, -1):
#	for i in range(get_child_count() - 1, connection.from_port, -1):
		move_from_connection.emit(connection.from, i, i + 1)

	change_slot_size.emit(slot, +1)


func _on_to_port_connected(connection: Dictionary) -> void:
	var slot_control: Control = get_left_slot_controls()[connection.to_port]
	var slot: BTSlot = slot_control.get_meta("left_slot")

	slot.is_to_port_connected = true


func _on_from_port_disconnected(connection: Dictionary) -> void:
	var slot_control: Control = get_right_slot_controls()[connection.from_port]
	var slot: BTSlot = slot_control.get_meta("right_slot")

	slot.is_from_port_connected = false

	if not slot.is_multiple:
		return

	var left_controls: Array[Node] = get_left_slot_controls()
	var right_controls: Array[Node] = get_right_slot_controls()

	change_slot_size.emit(slot, -1)
#	for i in range(get_child_count() - 1, connection.from_port, -1):
	for i in range(right_controls.size() - 1, connection.from_port, -1):
		move_from_connection.emit(connection.from, i, i - 1)
#	for i in range(left_controls.size() - 1, connection.from_port, -1):
#		move_to_connection.emit(connection.to, i, i - 1)


func _on_to_port_disconnected(connection: Dictionary) -> void:
	var slot_control: Control = get_left_slot_controls()[connection.to_port]
	var slot: BTSlot = slot_control.get_meta("left_slot")

	slot.is_to_port_connected = false

#endregion Event methods
