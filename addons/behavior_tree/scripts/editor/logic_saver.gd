class_name BTLogicSaver extends Node


var logic_editor: BTLogicEditor
var logic: BTLogic


func _init(logic_editor: BTLogicEditor) -> void:
	self.logic_editor = logic_editor


func save(path: String) -> void:
	logic = BTLogic.new()

	for graph_node in logic_editor.get_children():
		if not graph_node is BTGraphNode:
			continue

		var bt_node: BTNode = graph_node.bt_node

		if bt_node is BTRootTimerNode:
			logic.root_timer_nodes.push_back(bt_node)
		elif bt_node is BTRootNode:
			logic.root_tick_nodes.push_back(bt_node)


func link_children(graph_node: BTGraphNode, depth: int = 0) -> void:
	var bt_node: BTNode = graph_node.bt_node

	for c in logic_editor.get_connection_list():
		if c.from == graph_node.name:
			if bt_node is BTRootNode:
				bt_node.nodes.push_back("")


func get_connections_from_graph_node() -> void:
	pass
