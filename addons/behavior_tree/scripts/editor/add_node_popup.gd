@tool
class_name AddNodePopup extends Popup


#region Signals

signal node_selected(name: String)

#region Signals


#region Properties

@onready var nodes_tree: Tree = $%NodesTree
@onready var search_input: LineEdit = $%SearchInput

var last_popup_position: Vector2i = Vector2i()

#endregion Properties


#region Virtual methods

func _init():
	visible = false


func _ready() -> void:
	nodes_tree.connect("item_activated", _on_item_activated)
	search_input.text_changed.connect(_on_search_input_text_changed)
	var root := nodes_tree.create_item(null)

#endregion Virtual methods


#region Methods

func popup_at(new_position: Vector2) -> void:
	position = new_position - Vector2(size) / 2
	last_popup_position = new_position
	popup()
	search_input.select_all()
	search_input.grab_focus()


func add_node(node: BTNode) -> void:
	var root: TreeItem = nodes_tree.get_root()
	var items: Array[TreeItem] = root.get_children()
	var group: TreeItem

	for item in items:
		if item.get_text(0) == node.group:
			group = item

	if not group:
		group = nodes_tree.create_item()
		group.set_text(0, node.group)

	var new_node_item: TreeItem = group.create_child()
	new_node_item.set_text(0, node.name)
	new_node_item.set_icon(0, node.icon)
	new_node_item.set_meta("node", node)


func filter_by_text(text: String) -> void:
	for group in nodes_tree.get_root().get_children():
		var hidden_items: int = 0

		for item in group.get_children():
			if text == "":
				item.visible = true
			elif item.get_text(0).to_lower().replace(" ", "").contains(text):
				item.visible = true
			else:
				item.visible = false
				hidden_items += 1

		if hidden_items == group.get_child_count():
			group.visible = false
		else:
			group.visible = true

#endregion Methods


#region Event methods

func _on_item_activated() -> void:
	var selected_item := nodes_tree.get_selected()

	if selected_item.get_child_count():
		return

	node_selected.emit(selected_item.get_text(0))
	hide()


func _on_search_input_text_changed(new_text: String) -> void:
	new_text = new_text.replace(" ", "").to_lower()
	filter_by_text(new_text)

#endregion Event methods
