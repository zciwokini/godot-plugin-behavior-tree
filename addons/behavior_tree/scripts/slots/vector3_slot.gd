class_name BTVector3Slot extends BTVariantSlot


func _init(name: String) -> void:
	self.name = name
	type = TYPE_VECTOR3
	icon = EditorInterface.get_editor_theme().get_icon("Vector3", "EditorIcons")


func get_controls_for(side: String) -> Array[Control]:
	if controls.size() == 0:
		var c: Control = load("res://addons/behavior_tree/scenes/slots/vector3_slot.tscn").instantiate()

		var label: Label = c.get_node("Name")
		label.text = name
		label.visible = true if name else false

		if not name:
			c.add_theme_constant_override("separation", 0)

		c.set_meta("left", side == "left")
		c.set_meta("slot", self)
		c.set_meta("right", side == "right")

		controls.push_back(c)

	return controls
