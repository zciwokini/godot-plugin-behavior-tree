class_name BTFloatSlot extends BTVariantSlot


func _init(name: String) -> void:
	self.name = name
	self.icon = EditorInterface.get_editor_theme().get_icon("float", "EditorIcons")
	self.type = TYPE_FLOAT


func get_controls_for(side: String) -> Array[Control]:
	if controls.size() == 0:
		var theme: Theme = EditorInterface.get_editor_theme()
		var c: Control = load("res://addons/behavior_tree/scenes/slots/variant_slot.tscn").instantiate()

		var label: Label = c.get_node("Name")
		label.text = name
		label.visible = true if name else false

		var line_edit: LineEdit = c.get_node("Value")
		line_edit.text_changed.connect(func(t): size_changed.emit())
		line_edit.visible = not is_to_port_connected

		c.set_meta("left", side == "left")
		c.set_meta("slot", self)
		c.set_meta("right", side == "right")
		#c.set_meta("color", self.color)
		to_port_connection_changed.connect(_on_to_port_connection_changed)
		controls.push_back(c)

	return controls
