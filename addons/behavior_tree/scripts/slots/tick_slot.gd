class_name BTTickSlot extends BTSlot


func _init() -> void:
	self.name = "Tick"
	self.type = 0
	var theme: Theme = EditorInterface.get_editor_theme()
	self.icon = theme.get_icon("Play", "EditorIcons")
	self.color = Color("red")
