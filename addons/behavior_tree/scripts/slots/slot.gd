class_name BTSlot extends Resource


signal size_changed
signal slot_count_changed
signal color_changed(color: Color)

signal from_port_connection_changed(is_connected: bool)
signal to_port_connection_changed(is_connected: bool)


var name: String = ""
var type: int = 0
var icon: Texture2D = null
var color: Color #= EditorInterface.get_editor_theme().get_color("font", "Label").lightened(0.5)

var controls: Array[Control] = []
var is_multiple: bool = false

var extra_slots: int = 0:
	set(v):
		extra_slots = v if v > 0 else 0
		slot_count_changed.emit()

var is_from_port_connected: bool = false:
	set(v):
		is_from_port_connected = v
		from_port_connection_changed.emit(v)

var is_to_port_connected: bool = false:
	set(v):
		is_to_port_connected = v
		to_port_connection_changed.emit(v)



func _init(name: String) -> void:
	self.name = name


func get_input_controls() -> Array[Control]:
	return get_controls_for("left")


func get_output_controls() -> Array[Control]:
	return get_controls_for("right")


func get_controls_for(side: String) -> Array[Control]:
	if controls.size() == extra_slots + 1:
		return controls

	for i in range(extra_slots + 1):
		if controls.size() > i:
			continue
		var c: Label = load("res://addons/behavior_tree/scenes/slots/slot.tscn").instantiate()
		if side == "right":
			c.horizontal_alignment = HORIZONTAL_ALIGNMENT_RIGHT

		if not i:
			c.text = name

		c.set_meta("slot", self)
		c.set_meta(side, is_multiple)
		controls.push_back(c)

	for i in range(extra_slots + 1, controls.size()):
		controls[i].queue_free()
	controls = controls.slice(0, extra_slots + 1)

	return controls


func set_type(type: int) -> BTSlot:
	self.type = type
	return self


func set_is_multiple(is_multiple: bool) -> BTSlot:
	self.is_multiple = is_multiple
	return self


func set_icon(icon: Texture2D) -> BTSlot:
	self.icon = icon
	return self
