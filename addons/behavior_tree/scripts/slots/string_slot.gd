class_name BTStringSlot extends BTVariantSlot


func _init(name: String) -> void:
	self.name = name
	self.icon = EditorInterface.get_editor_theme().get_icon("String", "EditorIcons")
	self.type = TYPE_STRING
