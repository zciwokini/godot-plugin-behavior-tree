@tool
extends Control


#region Properties

@onready
var editor: Control = $Editor

@onready
var make_floating_button: Button = $%MakeFloatingButton

@onready
var window: Window = $Window

@onready
var placeholder: Control = $%Placeholder

@onready
var placeholder_return_button: Control = $%ReturnEditorButton

@onready
var placeholder_focus_button: Control = $%FocusEditorButton

#endregion Properties


#region Virtual methods

func _ready() -> void:
	make_floating_button.pressed.connect(editor_state_toggle)
	placeholder_focus_button.pressed.connect(window.grab_focus)
	placeholder_return_button.pressed.connect(editor_state_toggle)
	window.close_requested.connect(editor_state_toggle)

#endregion Virtual methods


#region Methods

func editor_state_toggle() -> void:
	var popout: bool = editor.get_parent() == self

	if popout:
		window.size = size
		window.position = get_screen_position()

	editor.reparent(window if popout else self, false)
	window.show() if popout else window.hide()

	if popout:
		window.grab_focus()

	make_floating_button.visible = not popout
	placeholder.visible = popout

#endregion Methods
