class_name BehaviorTree extends Node
## The edited BehaviorTreeLogic that is edited by the user.
## The resource is only loaded once, no matter how many times used.


#region Properties

@export var logic: BTLogic
@export var target: Node
@export var shared_data: Dictionary = {}

@export var start_on_ready: bool = true
@export var select_owner_as_actor: bool = false

#endregion Properties


#region Signals

signal started

#endregion Signals


#region Virtual methods

func _ready() -> void:
	set_physics_process(false)

	if select_owner_as_actor:
		target = owner

	if start_on_ready and logic != null:
		start()


func _physics_process(delta: float) -> void:
	for node in logic.root_physics_nodes:
		node.tick(target, shared_data)

#endregion Virtual methods


#region Methods

func start() -> void:
	_setup_root_timers()
	if logic.root_physics_nodes.size():
		set_physics_process(true)
	started.emit()


func _setup_root_timers() -> void:
	for root_timer in logic.root_timer_nodes:
		var timer := Timer.new()
		timer.wait_time = root_timer.time
		timer.autostart = true
		timer.connect(
			"timeout",
			func():
				for node in root_timer.nodes:
					node.tick(target, shared_data)
		)
		add_child(timer)

#endregion Methods
